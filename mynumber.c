#include <stdio.h>

int main()
{
   int i, j;
  
   for(i=1;i<101;i++)
   {
      if(i%2!=0)
         printf("%d ", i);
   }
   printf("\n");
   for(i=1;i<101;i++)
   {
      if(i%2==0)
         printf("%d ", i);
   }
   printf("\n");
   for(i=1;i<101;i++)
   {
    for(j=2;j<=i;j++)
    {
       if(i%j==0)
         break;
    }
    if(j==i && j==2)
      printf("%d ", i);
    else if(j==i)
      printf("%d ", i);
   } 
      
   return 0;
}
